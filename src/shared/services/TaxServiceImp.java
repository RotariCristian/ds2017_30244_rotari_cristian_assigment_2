package shared.services;

import shared.entities.Car;

public class TaxServiceImp implements TaxService {
    public int computeTax(Car car) {
        int sum;
        if(car.getEngineSize()<1600){
            sum=8;
        }else if(car.getEngineSize()<2000) {
            sum=18;
        } else if(car.getEngineSize()<2600){
            sum=72;
        } else if(car.getEngineSize()<3000){
            sum=144;
        } else {
            sum =290;
        }
         return (car.getEngineSize()/200)*sum;
    }
}
