package shared.services;

import shared.entities.Car;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface TaxService extends Remote {

    int computeTax(Car car) throws RemoteException;
}
