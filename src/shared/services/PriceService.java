package shared.services;

import shared.entities.Car;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface PriceService extends Remote {

    double computeSellingPrice(Car c) throws RemoteException;
}
