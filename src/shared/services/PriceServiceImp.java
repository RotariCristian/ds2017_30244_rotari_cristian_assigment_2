package shared.services;

import shared.entities.Car;

public class PriceServiceImp implements PriceService {
    public double computeSellingPrice(Car car) {
        return car.getPurchasingPrice() - (car.getPurchasingPrice()/7)*(2015-car.getYear());
    }
}
