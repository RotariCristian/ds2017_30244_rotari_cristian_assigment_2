package server;

import shared.services.PriceService;
import shared.services.PriceServiceImp;
import shared.services.TaxService;
import shared.services.TaxServiceImp;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

public class Server {

    public Server() {}

    public static void main(String args[]) {
        try {
            // Instantiating the implementation class
            PriceServiceImp price = new PriceServiceImp();
            TaxServiceImp tax = new TaxServiceImp();

            LocateRegistry.createRegistry(1099);
            // Exporting the object of implementation class
            // (here we are exporting the remote object to the stub)
            PriceService priceStub = (PriceService) UnicastRemoteObject.exportObject(price, 0);
            TaxService taxStub = (TaxService) UnicastRemoteObject.exportObject(tax, 0);

            // Binding the remote object (stub) in the registry
            Registry registry = LocateRegistry.getRegistry();

            registry.bind("price", priceStub );
            registry.bind("tax", taxStub );
            System.out.println("Server ready");
        } catch (Exception e) {
            System.out.println("Server exception: " + e.toString());
            e.printStackTrace();
        }
    }
}
