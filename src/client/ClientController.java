package client;

import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import shared.entities.Car;
import shared.services.PriceService;
import shared.services.TaxService;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;


public class ClientController {

    public Label taxLabel;
    public TextField yearField;
    public TextField priceField;
    public TextField sizeField;

    public void computeTax() {
        int size;
        try{
            size = Integer.parseInt(sizeField.getText());
            if(size<=0) {
                throw new NumberFormatException();
            }
            Car car = new Car();
            car.setEngineSize(size);
            Registry registry = LocateRegistry.getRegistry(null);

            // Looking up the registry for the remote object
            TaxService taxStub = (TaxService) registry.lookup("tax");

            // Calling the remote method using the obtained object
            this.taxLabel.setText(String.valueOf(taxStub.computeTax(car)));
        }catch (NumberFormatException e){
            taxLabel.setText("Invalid Car Details");
        }
        catch (Exception e){
            e.printStackTrace();
        }

    }

    public void computePrice() {
        int size;
        double price;
        int year;
        try{
            size = Integer.parseInt(sizeField.getText());
            price = Double.parseDouble((priceField.getText()));
            year = Integer.parseInt(yearField.getText());

            if(size<=0 || year <1900 || price<=0) {
                throw new NumberFormatException();
            }

            Car car = new Car(year, size, price);

            Registry registry = LocateRegistry.getRegistry(null);

            // Looking up the registry for the remote object
            PriceService priceStub = (PriceService) registry.lookup("price");

            // Calling the remote method using the obtained object
            this.taxLabel.setText(String.valueOf(priceStub.computeSellingPrice(car)));
        } catch (NumberFormatException e){
            taxLabel.setText("Invalid Car Details");
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }
}
